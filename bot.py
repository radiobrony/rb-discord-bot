import re
import asyncio
import logging
import discord
import configparser

from bwutils import fetch_np, dns_np, send_raindrop
from discord.ext import commands

# Setup logger
logger = logging.getLogger('discord')
logger.setLevel(logging.INFO)
handler = logging.FileHandler(filename='discord.log', encoding='utf-8', mode='w')
handler.setFormatter(logging.Formatter('@%(name)s [%(levelname)s] %(asctime)s: %(message)s'))
logger.addHandler(handler)

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')

# Setup discord-stuff
intents = discord.Intents.default()
intents.members = True
description = 'Je suis Blue Wave, mascotte de Radio Brony !'
bot = commands.Bot(command_prefix='!', description=description, intents=intents)

alerted = False


# On bot login
@bot.event
async def on_ready():
    print('Connecté en tant que ' + bot.user.name)
    print('ID: ' + str(bot.user.id))
    print('------------------')
    # Import our 'modules'
    bot.load_extension('radio')
    bot.load_extension('utilitaires')
    bot.load_extension('fun')


# On new messages
@bot.event
async def on_message(message):
    # Save links from team channel
    if str(message.channel.id) == conf.get('raindrop', 'chan') and \
       message.type == discord.MessageType.default:
        ignore = ['discordapp.com', 'discord.com', 'discord.gg', 'radiobrony.', 'tenor.com']
        links = re.findall(r'(https?://\S+)', message.content)
        items = []

        clean_message = message.content
        for link in links:
            clean_message = clean_message.replace(link, '[link]')

        try:
            i = 0
            for link in links:
                if any(thing in message.content for thing in ignore):
                    continue

                items.append({
                    'collection': {
                        '$id': conf.get('raindrop', 'collection')
                    },
                    'link': link,
                    'tags': [
                        'Discord:' + message.author.name,
                        message.channel.name
                    ],
                    'note': clean_message,
                    'order': i,
                    'pleaseParse': {}
                })

                i += 1

            if len(items) > 0:
                send_raindrop(conf, items)
        except:
            pass

    # Launch commands if there's any
    await bot.process_commands(message)


async def update_np():
    global alerted

    try:
        await bot.wait_until_ready()
        await asyncio.sleep(10)
        prev_np = ''

        while not bot.is_closed():
            try:
                rb_json = fetch_np()

                if 'title' not in rb_json['icestats']['source']:
                    continue

                np = rb_json['icestats']['source']['title']

                if prev_np != np:
                    prev_np = np

                    await bot.change_presence(activity=discord.Activity(
                        name=np,
                        type=discord.ActivityType.listening
                    ))

                    if 'Radio Brony - ' in np and 'hors-ligne' in np and not alerted:
                        chan = bot.get_channel(int(conf.get('bot', 'teamchan')))
                        await chan.send(
                            '<@{}> La radio est **hors-ligne** !'.format(conf.get('bot', 'admin'))
                        )
                        alerted = True
                    elif 'Radio Brony - ' not in np and 'hors-ligne' not in np:
                        alerted = False

                    dns_np(conf, np)

                await asyncio.sleep(10)
            except:
                print('update_np had an exception')
                try:
                    await bot.change_presence(
                        activity=discord.CustomActivity('être DJ sur Radio Brony')
                    )
                    await asyncio.sleep(30)
                except:
                    raise
    except:
        exit(1)


# Launch
if __name__ == '__main__':
    bot.loop.create_task(update_np())
    bot.run(conf.get('bot', 'token'))
