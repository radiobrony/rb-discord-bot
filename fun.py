import aiohttp
import configparser
from bwutils import del_message
from discord.ext import commands

# Setup config
conf = configparser.ConfigParser()
conf.read('./config.ini')


class Fun(commands.Cog):
    def __init__(self, bot):
        print('extension: fun')
        self.bot = bot

    def is_me(self, m):
        return m.author == self.bot.user

    @commands.command(description='Miaou !')
    async def meow(self, ctx):
        """Miaou !"""
        author = ctx.author

        try:
            async with ctx.channel.typing():
                async with aiohttp.ClientSession() as sess:
                    async with sess.get('http://aws.random.cat/meow') as r:
                        if r.status == 200:
                            js = await r.json()
                            await ctx.send('%s : %s' % (author.mention, js['file']))
                        else:
                            await ctx.send(
                                '%s : Le service est hors-ligne :(' % author.mention
                            )
                await del_message(self, ctx)
        except Exception as e:
            await ctx.send('%s : Je sais pas comment, mais un bug est arrivé.' % author.mention)
            print('>>> ERROR meow ', e)


def setup(bot):
    bot.add_cog(Fun(bot))
