import json
import html
import requests
import urllib.error
import urllib.parse
import urllib.request


# Helpers
def is_admin(ctx):
    return ctx.channel.permissions_for(ctx.author).manage_messages


def fetch_np():
    ice_api = 'https://radiobrony.fr/ice2.json?current=aac'

    rbreq = urllib.request.Request(ice_api, headers={'User-Agent': 'Blue Wave Discord Bot'})
    rbfile = urllib.request.urlopen(rbreq).read()
    return json.loads(rbfile.decode("utf-8"))


def dns_np(conf, title):
    cf_api = 'https://api.cloudflare.com/client/v4/zones/'
    cf_api += conf.get('cf', 'zone') + '/dns_records/' + conf.get('cf', 'entry')

    payload = {
        'content': title,
        'ttl': 60
    }
    head = {
        'X-Auth-Email': conf.get('cf', 'email'),
        'X-Auth-Key': conf.get('cf', 'key'),
        'Content-Type': 'application/json'
    }

    r = requests.request('PATCH', cf_api, json=payload, headers=head)

    if r.status_code != 200:
        print('No update to DNS?')

    return r


def send_raindrop(conf, items):
    url = 'https://api.raindrop.io/rest/v1/raindrops'
    payload = {
        'items': items
    }
    head = {
        'Authorization': 'Bearer ' + conf.get('raindrop', 'token'),
        'Content-Type': 'application/json'
    }

    r = requests.request('POST', url, json=payload, headers=head)

    if r.status_code != 200:
        print('Could not send raindrops?')

    return r


def html2md(text):
    # Un-Escape
    text = html.unescape(text)

    # Formating
    text = text.replace('<b>', '**').replace('<i>', '_')  # opening tags with formating code
    text = text.replace('</b>', '**').replace('</i>', '_')  # closing tags with reset code
    text = text.replace('<br>', "  \n").replace('<br/>', "  \n")  # and returns

    # Remove website stuff
    text = text.replace(' on DeviantArt', '').replace(' | Free Listening on SoundCloud', '')

    # OK done?
    return text


async def del_message(self, ctx):
    if ctx.guild:
        await ctx.message.delete()
