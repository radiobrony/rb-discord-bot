from bwutils import del_message, fetch_np
from discord.ext import commands


class Radio(commands.Cog):
    def __init__(self, bot):
        print('extension: radio')
        self.bot = bot

    @commands.command(description='Partage le titre en cours.')
    async def rb(self, ctx):
        """Partage le titre en cours."""
        author = ctx.author

        try:
            async with ctx.channel.typing():
                rbjson = fetch_np()
                await ctx.send(
                    '(%s) Actuellement sur Radio Brony : *%s* (%d auditeurs)' %
                    (author.mention, rbjson['icestats']['source']['title'],
                     rbjson['icestats']['source']['listeners'])
                )
                await del_message(self, ctx)
        except Exception as e:
            await ctx.send('%s : Aucune info disponible pour le moment. :confused:' % author.mention)
            print('>>> ERROR np ', e)


def setup(bot):
    bot.add_cog(Radio(bot))
