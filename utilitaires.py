from bwutils import del_message
from discord.ext import commands


class Utilitaires(commands.Cog):
    def __init__(self, bot):
        print('extension: utilitaires')
        self.bot = bot

    def is_me(self, m):
        return m.author == self.bot.user

    @commands.command(description='PONG!')
    async def ping(self, ctx):
        """PONG!"""
        author = ctx.author

        try:
            async with ctx.channel.typing():
                await ctx.send('%s : PONG !' % author.mention)
                await del_message(self, ctx)
        except Exception as e:
            await ctx.send('%s : Je sais pas comment, mais un bug est arrivé.' % author.mention)
            print('>>> ERROR Ping ', e)


def setup(bot):
    bot.add_cog(Utilitaires(bot))
