# Blue Wave
## Bot Discord pour le serveur de Radio Brony

### Installation
- Il vous faut Python 3.8+
- Ayez `libopus-dev`, `libffi-dev` et `ffmpeg` d'installé sur la machine sur Linux
- Créez une application ici et transformez-le en bot : https://discordapp.com/developers/applications/me
- Clonez ce répo : `git clone https://gitlab.com/radiobrony/rb-discord-bot.git`
- Insallez les dépendances : `pip3 install -U -r requirements.txt`
- Copiez le config d'example vers `config.ini` : `cp config.ini.dist config.ini`
- Editez le fichier et ajoutez le token de votre bot à la variable `token`
  **Notes :** Ne mettez pas les valeurs entre guillemets dans le .ini
- Invitez le bot dans le salon (remplacez `[client id]` par celui du bot) : `https://discordapp.com/oauth2/authorize?client_id=[client id]&scope=bot`
- Lancez le bot (mais pas trop fort ou trop loin) : `python3 bot.py`
- Donnez au bot les droits nécessaires (voire ajoutez-lui dans un groupe)

### Commandes
#### Utilisateurs

- `!np` : Affiche le titre en cours et le nombre d'auditeurs
- `!ping` : PONG !
- `!meow` : Envoie une image de chat au hasard

#### Administrateurs

- `!music` : Invite le bot dans le salon vocal où vous êtes pour jouer le stream
- `!silence` : Déconnecter le bot du salon vocal
- `!vol <nbr>` : Changer le volume du bot (0-100)

### Licence
[MIT © Radio Brony](LICENSE)
